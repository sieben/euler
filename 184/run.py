#!/usr/bin/env python3
# -*- coding:utf8 -*-

# All possible triangles fall into two types:
# (1) two vertices in the same quadrant, the third in the opposite quadrant;
# (2) all three vertices in different quadrants.
# The number of triangles of the first type is
# four times sum by slope m of c1(m) * c(m) * c2(m), where
# c1 is the number of points with slope < m;
# c is the number of points with slope = m;
# c2 is the the number of points with slope > m.
# The number of thriangles of type 2 is four
# times the number of pairs of points in a quadrant that do not have
# the same slope.

from fractions import Fraction

def count_triangles(r):
    r2 = r * r
    n, slopes = 0, {}

    # Tabulate the number of points in the first quadrant by slope:
    for x in range(1, r):
        for y in range(r):
            rr = x*x + y*y
            if rr < r2:
                n += 1
                m = Fraction(y, x)
                if m in slopes:
                    slopes[m] += 1
                else:
                    slopes[m] = 1

    t1 = 0 # number of triangles of type 1
    c1 = 0
    sameSlopePairs = 0

    # Sort by slope and traverse:
    for m in sorted(slopes):
        c = slopes[m]
        c2 = n - c1 - c
        t1 += c1 * c * c2
        sameSlopePairs += c*c
        c1 += c

    return 4 * (t1 + n * (n * n - sameSlopePairs)//2)

if __name__ == "__main__":
    print(count_triangles(3))  # Answer: 360
    print(count_triangles(5))  # Answer: 10600
    print(count_triangles(105))  # Answer: 1725323624056
