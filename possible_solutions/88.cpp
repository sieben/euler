#include <cstdio>
#include <cstdlib>

#include <iostream>

#include <map>
#include <set>

typedef unsigned long long entier;

entier somme(const std::set<entier> & mySet)
{
	entier result = 0;
	std::set<entier>::const_iterator it;
	std::set<entier>::const_iterator en = mySet.end();
	for (it = mySet.begin(); it != en; ++it)
	{
		result += *it;
	}
	
	return result;
}

void print(const std::set<entier> & mySet)
{
	entier result = 0;
	std::set<entier>::const_iterator it;
	std::set<entier>::const_iterator en = mySet.end();
	for (it = mySet.begin(); it != en; ++it)
	{
		std::cout << *it ", ";
	}
	
	std::cout << std::endl;
	
	return result;
}

std::vector<entier> factor(entier myNumber)
{
	std::vector<entier> result;
	
	entier diviseur = 2;
	
	while (myNumber > diviseur)
	{
		if (myNumber % diviseur == 0)
		{
			myNumber /= diviseur;
			result.push_back(diviseur);
		}
		else
		{
			++diviseur;
		}
	}
	
	if (myNumber != 1)
		result.push_back(myNumber);
	
	return result;
}

int main(int argc, char** argv)
{
	if (argc < 2)
		return 1;
	entier k_limit = atoi(argv[1]);
	
	std::set<entier> result;
	
	for (entier k = 2; k < = k_limit; ++k)
	{
		std::cout << "k = " << k << std::endl;
		
	}
	
	print(result);
	
	std::cout << somme(result) << std::endl;
	
	return 0;
}
