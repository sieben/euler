import Data.Array
 
triplets = 
    [p | 
    n <- [2..706],
    m <- [1..n-1],
    gcd m n == 1, 
    let p = 2 * (n^2 + m*n),
    odd (m + n),
    p <= 10^6
    ]
 
hist bnds ns = 
    accumArray (+) 0 bnds [(n, 1) |
        n <- ns,
        inRange bnds n
        ]
 
problem_75 = 
    length $ filter (\(_,b) -> b == 1) $ assocs arr
    where
    arr = hist (12,10^6) $ concatMap multiples triplets
    multiples n = takeWhile (<=10^6) [n, 2*n..]

main = do
	print $ problem_75
