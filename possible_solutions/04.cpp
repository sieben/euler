#include <string>
#include <cstdlib>
#include <cstdio>

int palindrome(long long int nb)
{
	char str[200];
	sprintf(str,"%lld",nb);
	int n = strlen(str);
	for(int i=0;i < (n/2);i++)
	{
		if (str[i] != str[n-1-i])
			return 0;
	}

	return 1;
}

int main()
{
	long long int max = 0;
	long long int n;
	for(long long int i=2;i < 1000;i++)
	{
		for (long long int j=1;j <= i;j++)
		{
			n = i*j;
			if (palindrome(n) == 1 && n > max)
				max = n;
			
		}
	}

	printf("%lld\n",max);

	return 0;

}
