#include <cstdio>
#include <cstdlib>

#include <map>

int main(int argc,char* argv[])
{
	if (argc != 2)
		return 1;

	long long int n = atoll(argv[1]);

	std::map<long long int,bool> m;
	std::map<long long int,bool>::iterator it_mobile, it_fixe;

	for (long long int i = 2;i < n;i++)
	{
		m.insert(std::pair<long long int,bool>(i,true));
	}
	
	it_fixe = m.end();

	for (it_mobile = m.begin();it_mobile != it_fixe;it_mobile++)
	{
		if (it_mobile->second)
		{
			for(long long int i=2*(it_mobile->first);i<n;i+=it_mobile->first)
			{
				m[i] = false;
			}
		}
	}

	long long int somme = 0;

	for (it_mobile = m.begin();it_mobile != it_fixe;it_mobile++)
	{
		if (it_mobile->second)
		{
			somme+=it_mobile->first;
		}
	}

	printf("%lld\n",somme);

	return 0;
}

