#include <cstdio>
#include <iostream>
#include <cstdlib>

#include <map>

typedef long long int nombre;
typedef std::map<nombre,nombre> carte;


nombre suivant(const nombre& n)
{
	if (n==1)
		return 1;
	else if (n%2 == 0)
		return n/2;
	else
		return (3*n+1);
}

nombre problem14(const nombre& max)
{
	carte m;

	for(nombre i=1,j=1;i<max;j++,i*=2)
	{
		m[i] = j;
	}

	nombre gagnant = 0, compteur_max = 0;

	for(nombre i=1;i<max;i++)
	{
		nombre current = i;
		nombre compteur = 1;
		while(current > 1)
		{
			carte::iterator f = m.find(current);
			if (f != m.end())
			{
				current = 1;
				compteur+=f->second;
			}
			else
			{
				current = suivant(current);
				compteur++;
			}
		}

		if (compteur > compteur_max)
		{
			compteur_max = compteur;
			gagnant = i;
		}
	}

	return gagnant;
}

int main(int argc, char* argv[])
{
	if (argc != 2)
		return 1;

	nombre n = atoll(argv[1]);
	nombre m = n;

	while (m != 1)
	{
		std::cout << m << "->";
		m = suivant(m);
	}

	std::cout << 1 << std::endl;


	nombre resultat = problem14(n);
	std::cout << n << " : " << resultat << std::endl;
	
	return 0;
}




