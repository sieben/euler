#include <iostream>
#include <string>

#include <map>
#include <list>
#include <vector>

typedef unsigned long long int nombre;

nombre power(nombre a, nombre b)
{
	if (b == 0)
		return 1;
	else if (b%2 == 0)
		return power(a*a,b/2);
	else
		return a*power(a,b-1);
}

int main(int argc,char* argv[])
{
	if (argc != 2)
		return -1;

	nombre limite = atoll(argv[1]);
	nombre limite_max = power(2,limite);
	nombre limite_min = power(2,limite/2);

	std::vector<bool> l1;
	std::vector<bool> l2;

	//bool* c = new bool[limite_min + 1];
	//bool* c2 = new bool[limite_max + 1];

	std::cout << "Init 1" << std::endl;
	l1.push_back(false);
	l1.push_back(false);
	for(nombre i = 2;i < limite_min;i++)
	{
		l1.push_back(true);
		//c[i] = true;
	}

	std::cout << "Init 2" << std::endl;
	l2.push_back(false);
	for(nombre i = 1;i < limite_max;i++)
	{
		l1.push_back(true);
		//c2[i] = true;
	}

	std::cout << "Algo" << std::endl;
	for(nombre i = 2;i < limite_min;i++)
	{
		if (l1[i])
		{
			for(nombre j = i*i;j<limite_min;j+=i)
			{
				l1[j] = false;
			}

			for(nombre j = i*i;j<limite_max;j+=i*i)
			{
				l2[j] = false;
			}
		}
	}

	std::cout << "Compteur" << std::endl;
	nombre compteur = 0;
	for(nombre i = 0; i < limite_max;i++)
	{
		// std::cout << it_m->first << " " << it_m->second << std::endl;
		 if (l2[i]) compteur++;
	}

	std::cout << "Total : " << compteur << std::endl;

	return 0;
}
