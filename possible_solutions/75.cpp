#include <sstream>
#include <iostream>
#include <stdio.h>
#include <map>
#include <stdlib.h>

int main(int argc, char** argv)
{
	if (argc != 2)
		return 1;

	std::map<long long,  long long> m;
	std::map< long long,  long long>::iterator it1, it2;

	long long limite = atoll(argv[1]);

	long long compteur = 0;
	
	for ( long long a = 1; 3 * a < limite; a++)
	{
		std::cout << "a : " << a << std::endl;

		for ( long long b = a + 1; 2 * b < limite; b++)
		{
			for ( long long c = b + 1; a + b + c <= limite; c++)
			{
				if (a*a + b*b == c*c)
				{
					// std::cout << a << " " << b << " " << c << std::endl;
					it1 = m.find(a+b+c);

					if (it1 == m.end())
					{
						m.insert(std::make_pair< long long,  long long>(a+b+c,1));
					}
					else
					{
						it1->second++;
					}
				}
			}
		}
	}

	for (it1 = m.begin(), it2 = m.end(); it1 != it2; ++it1)
	{
		if (it1->second == 1)
		{
			compteur ++;
			//std::cout << it1->first << std::endl;
		}
	}

	std::cout << "Compteur : " << compteur << std::endl;	

	return 0;
}
/*
#! /usr/bin/env python
#-*- coding: utf-8 -*-

import math
import sys
import string
import copy
import time

def rectangle(a,b):
	c = int(math.sqrt(a**2 + b**2))
	if a**2 + b**2 == c**2:
		return c
	else:
		return -1

def problem75():
	limite = int(sys.argv[1])
	#slimite = int(math.sqrt(limite) + 1)
	triangle = {}
	for (a,b,c) in [(a,b,c) for a in xrange(limite/3) for b in xrange(limite/2) for c in xrange(limite/2) if a + b + c <= limite and a < b and b < c]:
		#print a,b,c
		if a**2 + b**2 == c**2:
			l = a + b + c
			if l in triangle:
				triangle[l] = triangle[l] + 1
			else:
				triangle[l] = 1
	compteur = 0
	#print triangle
	for k,v in triangle.iteritems():
		if v == 1:
			compteur = compteur + 1
	print 'compteur :', compteur

problem75()
*/
