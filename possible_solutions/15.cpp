#include <iostream>

typedef unsigned long long int nombre;

nombre factoriel(nombre n)
{
	if (n<1)
		return 0;
	else if (n == 1)
		return 1;
	else 
		return (n * factoriel(n-1));
}

nombre C(nombre n, nombre p)
{
	if (p > n)
		return 0;
	if (p == n || p == 0)
		return 1;
	return factoriel(n)/(factoriel(n-p)*factoriel(p));
}

int main(int argc, char* argv[])
{
	if (argc != 2)
		return 1;

	nombre n = atoll(argv[1]);

	nombre somme=0;

	for(nombre p=0;p<=n;p++)
	{
		nombre c = C(n,p);
		somme+=c*c;
	}

	std::cout << somme << std::endl;

	return 0;
}


