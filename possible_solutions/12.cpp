#include <cstdlib>
#include <cstdio>
#include <iostream>

#include <map>

typedef unsigned long long int nombre;
typedef std::map<nombre,nombre> facteurs;
typedef facteurs::iterator facteur;

facteurs factorise(nombre n)
{
	nombre i = 2;
	facteurs m;
	while(n > 1)
	{
		if (n%i == 0)
		{
			n/=i;
			m[i]++;
		}
		else
		{
			i++;
		}
	}

	return m;
}

nombre diviseurs(nombre n)
{
	nombre d = 1;

	facteurs m = factorise(n);

	for(facteur it_mobile = m.begin(),it_fixe = m.end();it_mobile != it_fixe;it_mobile++)
	{
		d = d * (it_mobile->second + 1);
	}	

	return d;
}

int main(int argc,char* argv[])
{
	if (argc != 2)
		return 1;

	nombre n = atoll(argv[1]);

	nombre compteur = 1;
	nombre triangle = 0;
	nombre max_diviseur = 0;
	nombre gagnant = 0;

	while(max_diviseur <= n)
	{
		triangle+=compteur;
		compteur++;

		nombre diviseur = diviseurs(triangle);
		if (diviseur > max_diviseur)
		{
			max_diviseur = diviseur;
			gagnant = triangle;

			std::cout << gagnant << " " << max_diviseur << std::endl;
		}
	}

	std::cout << "gagnant : " << gagnant << std::endl;

	return 0;
}

