import Data.List
isSquare x = 
    (truncate $ sqrt $ fromIntegral x)^2 == x
 
cube m = 
    sum [ (a`div`2) - if a > m then (a - m -1) else 0|
    a <- [1..2*m],
    isSquare ((a)^2 + m2)
    ]
    where
    m2 = m * m
 
problem_86 =
    findIndex (>1000000) (scanl (+) 0 (map cube [1..]))
    
main = do
	print $ problem_86
