import Data.Char

intsqrt m = floor (sqrt $ m)

main =
    sum [f x |
    a <- [1..100],
    x <- [intsqrt $ a * t],
    x * x /= a * t
    ]
    where
    t=10^202
    f = (sum . take 100 . map (flip (-) (ord '0') .ord) . show)
