#include <iostream>
using namespace std;
int a[101][101];

int f(int k, int ub)
{
	bool fill_in = a[k][ub] == -1;
	if(!fill_in)
	{
		return(a[k][ub]);
	}
	if(k == 1)
	{
		return(1);
	}
	ub = min(ub, k); //to simplify things further down
	int rt = ub == k ? 1 : 0;

	for(int i = 1; i <= ub; i++)
	{
		if(k - i != 0)
		{
			rt += f(k - i, i);
		}
	}
	if(fill_in)
	{
		a[k][ub] = rt;
	}
	return(rt);
}

int main()
{
	for(int k = 0; k < 101; ++k)
	{
		for(int j = 0; j < 101; ++j)
		{
		a[k][j] = -1;
		}
	}
	cout << f(100, 99) << endl;

	return(0);
}
