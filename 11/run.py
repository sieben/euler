#!/usr/bin/env python3
# -*- coding:utf-8 -*-

if __name__ == "__main__":
    res = int()
    data_input = open('grid.txt')
    grid = list()
    for line in data_input:
        grid.append(line.strip('\n').split(' '))

    for i in range(20):
        for j in range(20):
            origin = int(grid[i][j])

            # Sous total horizontal
            if j < 17:
                tot_horizontal = origin
                for k in range(1, 4):
                    tot_horizontal *= int(grid[i][j + k])

                if tot_horizontal > res:
                    res = tot_horizontal

            # Sous total vertical
            if i < 17:
                tot_vertical = origin
                for k in range(1, 4):
                    tot_vertical *= int(grid[i + k][j])
                if tot_vertical > res:
                    res = tot_vertical

            # Sous total diagonal
            if 2 < i < 17 and 2 < j < 17:
                tot_diagonal = origin
                for k in range(1, 4):
                    tot_diagonal *= int(grid[i + k][j - k])
                if tot_diagonal > res:
                    res = tot_diagonal
            if i < 17 and j < 17:
                tot_diagonal = origin
                for k in range(1, 4):
                    tot_diagonal *= int(grid[i + k][j + k])
                if tot_diagonal > res:
                    res = tot_diagonal
    print(res)
